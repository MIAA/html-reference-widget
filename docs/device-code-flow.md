## Device code flow

The device code flow requires three templates:

| URL Anatomy | Template | Description |
| ----------- | -------- | ----------- |
| `${policyGateUrl}/device/confirm` | [confirmDevice.ejs](./src/confirmDevice.ejs) | Enter the user code you see on the device |
| `${policyGateUrl}/device/confirm_resume` | (./src/confirmDeviceResume.ejs) | Confirm the user code and any device information |
| `${policyGateUrl}/device/confirm_resume` | (./src/confirmDeviceSuccess.ejs) | The device has correctly been confirmed |

The `confirmDeviceResume.ejs` template in this repository shows the IP address and User Agent
of the device. It is possible to show more information, but then you need to provide a custom
function that extracts this information from the request. Here is an example:

```
getDeviceInfo(req) {
    return {
        ip: req.ip,
        userAgent: req.get("user-agent"),
        serialNumber: req.body.serialNumber
     };
}
```

When the device requests a device code, it must then include the `serialNumber` in the body (x-www-form-urlencoded, so nested properties are not allowed, e.g. `req.body.device.number`).