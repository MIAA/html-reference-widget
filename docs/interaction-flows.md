# Interaction flows
All of these user flows are *unauthenticated* ones, i.e. the user is anonymous at the start of the flow, and may be authenticated at the end of it. An interaction is started by navigating the browser to PolicyGate's `/auth` endpoint, as is described in the relevant documentation.

Below you can find a diagram of the interaction flow decisions that PolicyGate considers when rendering a screen. This can help you in clarifying when a certain screen is shown and how to trigger it.

 ![PolicyGate Interaction Flow](/docs/images/interaction_flow.png)

## Summary

| URL Anatomy | Template | Description |
| ----------- | -------- | ----------- |
| `${policyGateUrl}/interaction/v2/${interactionUuid}/login` | [login.ejs](./src/login.ejs) | Log in to your traditional (with email and password) or social (with a social provider) account or passwordless (webauthn) |
| `${policyGateUrl}/interaction/v2/${interactionUuid}/register` | [register.ejs](./src/register.ejs) | Register a new traditional account |
| `${policyGateUrl}/interaction/v2/${interactionUuid}/register/success` | [registerSuccess.ejs](./src/registerSuccess.ejs) | Success screen shown after the registration was completed. This screen is only shown when email verification is required for the client that started the interaction. |
| `${policyGateUrl}/interaction/v2/${interactionUuid}/invitation` | [invitation.ejs](./src/invitation.ejs) | complete the invitation process for a provisional account |
| `${policyGateUrl}/interaction/v2/${interactionUuid}/forgotPassword` `${policyGateUrl}/interaction/v2/${interactionUuid}/forgotPassword/success` | [forgotPassword](./src/forgotPassword.ejs) [forgotPasswordSuccess](./src/forgotPasswordSuccess.ejs) | Request an email to reset your password in case you forgot it  |
| `${policyGateUrl}/interaction/v2/${interactionUuid}/resetPassword` | [resetPassword.ejs](./src/resetPassword.ejs) | Reset your password as a result of receiving an email |
| `${policyGateUrl}/interaction/v2/${interactionUuid}/resetPassword/success` | [resetPasswordSuccess.ejs](./src/resetPasswordSuccess.ejs) | When enabled, shows you a success screen after successfully updating the password. |
| `${policyGateUrl}/interaction/v2/${interactionUuid}/consent` | [consent.ejs](./src/consent.ejs) | Give consent to agree/reject requested scopes |
| `${policyGateUrl}/interaction/v2/${interactionUuid}/emailVerification` `${policyGateUrl}/interaction/v2/${interactionUuid}/emailVerification/success` `${policyGateUrl}/interaction/v2/${interactionUuid}/emailVerification/process` | [emailVerification.ejs](./src/emailVerification.ejs) [emailVerificationSuccess.ejs](./src/emailVerificationSuccess.ejs) [emailVerificationProcess.ejs](./src/emailVerificationProcess.ejs) | Request an email for verifying your account |
| `${policyGateUrl}/interaction/v2/${interactionUuid}/webauthn/challenge` |  | Get a challenge for WebAuthn interaction |
| `${policyGateUrl}/interaction/v2/${interactionUuid}/webauthn/credentials` | [addWebAuthn.ejs](./src/addWebAuthn.ejs) | Add WebAuthn credentials |
| `${policyGateUrl}/interaction/v2/${interactionUuid}/webauthn/assertion` |  | Login with WebAuthn |
| `${policyGateUrl}/interaction/v2/${interactionUuid}/mfa/passCode` | [passCodeMFA.ejs](./src/passCodeMFA.ejs)  | PassCode MFA Verification (enter a pass code received by the user) |
___

## Templates

### Login
The login screen is the default screen that gets rendered when an authorization request is started and no other screen is specified. It contains a simple username/password form that needs to be submitted to continue with the interaction. In this form you can specify "remember me". For more info about the remember me functionality look here: [Controlling session persistency (remember me)](#controlling-session-persistency-(remember-me)).

### Register
The registration screen is used to register a new account. The screen contains a form that will be submitted to ProfileConnect. In ProfileConnect validation and mapping towards the identity store schema is performed. Any errors from ProfileConnect are handled by re-rendering the screen with the error in the InteractionDetails.

### Register Success
The register success screen is an alternative version of the "emailVerification" screen. It is only rendered when `email_verification_required=true` and when coming from the registration screen. All subsequent login attempt that still require email verification will render the "emailVerification" screen instead.

### Invitation
The invitation screen is similar to the registration screen but differs in the fact that it requires an "invitation" request_token to be provided in order to render the screen. This "invitation" request_token needs to be created prior and can contain information about the user that is trying to register. Completing the registration upgrades the user profile from "provisional" to "full".

### Forgot Password
The forgot password screen can be used to trigger a "password reset" mail. On this screen the user needs to submit a form containing an email address. Once submitted correctly, PolicyGate will render the forgotPasswordSuccess screen where you can show a success message to the end-user.

### Forgot Password Success
The forgot password success screen is only rendered after the forgot password screen was submitted successfully. The screen can be used to show a "success" message to the end-user and to redirect the user back to e.g. the previous screen or the login screen.

### Reset Password
The reset password screen is rendered when an authorization request is started that contains a "reset_password" request_token. This token allows a user to reset the password associated with the account that the request_token was made for. When submitting the form, PolicyGate hands over the data to ProfileConnect for validation. Depending on the password-manager settings configured, the password might need to follow certain rules. See [Password Manager](#password-manager) for more info.

After successfully updating the password there are two ways the flow can continue. Either we show the "reset password success" screen or we finish the interaction. To enable showing the reset password success screen make sure to enable it in client settings: 
> `reset_password_success_enabled: true`

In addition if you want to skip verification of the newly saved credentials you can also enable:
> `reset_password_success_skip_auth: true`

The previous setting allows you to enable/disable verifying whether or not the user will actually be able to log in with the new credentials.

### Reset Password Success
The reset password success screen is rendered after the password reset form has been submitted successfully and the client setting `reset_password_success_enabled=true` is set in config. You can use this screen to show a success message to the user and to redirect him back to login.

### Consent
> Note: Consents are disabled by default. To enable it Consent Management needs to be enabled for your tenant. Additionally the client settings need to be configured with which consents need to be taken into account when deciding whether or not to show this screen. For more info consult the Consent Management documentation.

The [consent screen](./src/consent.ejs) is shown when the configuration requires a user to give consent. 
PolicyGate differentiates between two different types of consent:
1. user-to-app consents (scope consents)
2. user-to-enterprise (enterprise consents)

User to app consents are always in relation to the scopes that are requested by the application that is communicating with PolicyGate. For example if an application wants access to the user profile it might request the "profile" scope. When Consent Management is enabled you can configure PolicyGate to first request user permission before it provides this scope to the application. This is where the consent screen becomes relevant.

The second type of consents are *user to enterprise*. These consents are often more general in nature and typically include things like terms and conditions, privacy policy, ...

Regardless of which type of consent is required, PolicyGate will show the consent screen if it detects that the user that is trying to log in does not yet have all of the required consents.

At that point the user can either accept or reject the consent. In some cases the user will be unable continue without first accepting a consent.

If he decides to accept the consent, the resulting token will contain the associated scopes (and claims) for that consent. If he decides to reject the consent, the token will not contain the associated scopes (and claims).

If advanced consent management is enabled the users' choices are saved and each subsequent request will use the previously saved values as input to decide whether or not consent is required. This also affects which scopes should be returned in the token.

The provided example of the consent screen contains some sample code of how these consents can be presented to the end user. If you're planning on changing this then make sure you use the same mechanics that are present in the example template to send both the hidden and updated values to the server. PolicyGate expects an object of key-value pairs where each key is a consent and each value is a boolean. true means "agreed", false means "rejected".

### Email Verification
> Note: email verification is disabled by default. To enable it add the following property to your client settings:

> `email_verification_required: true`


After sending your email through the `emailVerification` template, an email will be sent. In this email there will be an URL of your landing page with a request_token parameter. Use this token in your landing page to make an auth request. The last parameters are important for reaching the `emailVerificationProcess` template.

Example auth request:
```
GET /auth
?response_type=id_token+token
&nonce={nonce}
&state={state}
&client_id={clientId}
&scope={scopes}
&redirect_uri={redirect_uri}
&show_screen=emailVerificationProcess
&request_token={request_token}
```

### Social interaction flows
Social interaction flows are a subset of the interaction flows, that come into play when the user logs in using a social provider. Upon logging in with a social provider, PolicyGate will receive a social profile of the user with a unique `$PID`. PolicyGate will then check if it can either find an account with this `$PID` or alternatively if it can find an account with the same email as the one on the social profile.

Based on this, there are four possible social flows:

- Social login: in case there's an existing account with the same `$PID`. It does not matter wether this account is a traditional or social one.
- Social registration: in case there's neither an account with the same `$PID`, nor one with the same email as the one on the social profile
- Traditional merge: in case there's no account with the same `$PID`, but there's a **traditional** one with the same email. In this flow, the user will need to confirm they own the traditional account, by logging in using email and password.
- Social merge: in case there's no account with the same `$PID`, but there's a **social** one with the same email. In this flow, the user will need to confirm they own the social account, by logging in using another social provider. PolicyGate automatically removes the social provider that was used to trigger this flow, forcing the user to select another social provider.

| URL Anatomy | Template | Description |
| ----------- | -------- | ----------- |
| `${policyGateUrl}/interaction/v2/${interactionUuid}/social/register` | [registerSocial.ejs](./src/registerSocial.ejs) | Register a new social account |
| `${policyGateUrl}/interaction/v2/${interactionUuid}/social/merge/traditional` | [mergeTraditional.ejs](./src/mergeTraditional.ejs) | Merge an existing traditional account with a social one, by logging in to the traditional one |
| `${policyGateUrl}/interaction/v2/${interactionUuid}/social/merge/social` | [mergeSocial.ejs](./src/mergeSocial.ejs) | Merge an existing social account with another social one, by logging in to the first |

### Register Social
The register social screen is shown when PolicyGate detects that the user trying to log in with a social provider is not yet found in the identity store.
PolicyGate will add all of the information it receives from the social identity provider into the "social_profile" property of the InteractionDetails.

Because each social identity provider can choose to send whatever information it wants back to PolicyGate, we cannot make any assumptions about the data we receive. However, PolicyGate has a couple of pre-defined mappings for some social identity providers. Below you can find how PolicyGate maps the data it receives from each of these providers.

#### Swiss RX
```
role
name_id
acc_id
acc_type
acc_grp
given_name
family_name
email
street_address
language
gln
unique_name
```
#### DocCheck
```
gender
title_salutation
street_address
postal_code
locality
country_id
country_code
phone_number
fax_number
occupation_profession_id
occupation_profession_parent_id
occupation_discipline_id
occupation_activity_id
given_name
family_name
email
```
### WebAuthn

WebAuthn allows users to login without a password. Instead an authenticator is used. This can be a fingerprint, facial recognition, a USB key, etc. Examples are Windows Hello, Yubikey, Apple's FaceID and TouchId, etc.

There are two flows: registering a new credential and verifying an assertion (i.e. logging in).

#### Registering a new credential

For a user to be able to log in with WebAuthn, we first need to store the WebAuthn credentials. This involves the following steps:

- GET a challenge from PolicyGate
- Let the client system create a credential
- POST the credential to PolicyGate

Getting the challenge is done by calling `GET ${policyGateUrl}/interaction/v2/${interactionUuid}/webauthn/challenge?client_id=${clientId}`. The result will be a HTTP 200 with a body:

```
{
    data: "TY3ODkwIiwibmFt...."
}
```

See [webauthn-script.ejs](./src/partials/webauthn-script.ejs) for more details.

Then we ask the client system to get an assertion. See [webauthn-script.ejs](./src/partials/webauthn-script.ejs) for more details.

When we get back our credential object, we can POST it to `${policyGateUrl}/interaction/v2/${interactionUuid}/webauthn/credentials` with a x-www-form-urlencoded form with the following parameters:

- uuid: the user uuid
- id: the credential id
- clientDataJSON: the clientDataJSON found in the credential object
- attestationObject: the attestationObject found in the credential object
- name: a human readable name (so that the user can delete it later)

In the reference files ([register.ejs](./src/register.ejs)), this is done by submitting a form with hidden fields. PolicyGate will then continue the interaction.

#### Verifying an assertion (logging in)

Logging in is done in these steps:

- Get a challenge from PolicyGate
- Get an assertion from the client system
- Post the assertion to PolicyGate

Getting the challenge is done by calling `GET ${policyGateUrl}/interaction/v2/${interactionUuid}/webauthn/challenge?client_id=${clientId}`. The result will be a HTTP 200 with a body:

```
{
    data: "TY3ODkwIiwibmFt...."
}
```

See [webauthn-script.ejs](./src/partials/webauthn-script.ejs) for more details.

Then we ask the client system to get an assertion. See [addWebAuthn.ejs](./src/addWebAuthn.ejs) for the code.

When we get back our assertion object, we can POST it to `${policyGateUrl}/interaction/v2/${interactionUuid}/webauthn/assertion` with a x-www-form-urlencoded form with the data from the assertion object:

- assertionId
- clientDataJSON
- userHandle
- signature
- authenticatorData

In the reference files ([login.ejs](./src/login.ejs)), this is done by submitting a form with hidden fields. PolicyGate will then continue the interaction.

#### Managing credentials

By using ProfileConnect, it is possible to request the WebAuthn credentials of a user and delete them. This can be used to give the user a list of WebAuthn credentials, listing them by the name they created when registering a new credential. They can then choose to delete credentials. This is important if they have lost their authenticator for example.
___

## Controlling session persistency (remember me)
By default a successful authentication attempt will result in a transient user session.
Transient in this context means that the session will be destroyed when the browser is fully shut down. While the browser is open the session remains active without an expiration date.

PolicyGate also supports persistent sessions. 
Persistent in this context means that the session will stay active in the current browser even after the browser has been restarted. It also means that the session will get refreshed when a new authentication attempt detects a valid session.


You can instruct PolicyGate to create a persistent session by including a "rememberMe" input field in the form before submitting a login/registration/... This input field should have a "truthy" value to indicate `true` and a "falsy" value to indicate `false`. By not including this field the default behaviour is to assume "rememberMe=false".
e.g.

```html
<form method="POST">
	<!-- ... other fields go here ... -->
	<label><input type="checkbox" name="rememberMe" value="yes" checked="yes">Stay signed in</label>
</form>
```