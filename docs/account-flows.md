# Account flows
In addition to the interaction flows, PolicyGate also offers a few *account management* flows out of the box. These flows are meant to update your credentials or terminate your account. There are no flows to update other profile related data, as this should be tackled by addressing the ProfileConnect API.

| URL Anatomy | Template | Description |
| ----------- | -------- | ----------- |
| `${policyGateUrl}/account/password` | [changePassword.ejs](./src/changePassword.ejs) | Change your password |
| `${policyGateUrl}/account/email` | (not yet supported) | Change your email |
| `${policyGateUrl}/account/forget` | (not yet supported) | Terminate your account or "forget me" |
| `${policyGateUrl}/account/success/${updatedProperty}` | [accountSuccess.ejs](./src/accountSuccess.ejs) |  |

All of these endpoints allow passing a `redirectUri` parameter. PolicyGate will redirect to this URI in two cases:
 - In case the user is not logged in, PolicyGate will redirect to `${redirectUri}?error=login_required`
 - In case the user completes the account flow, PolicyGate will redirect to the `${policyGateUrl}/account/success/${updatedProperty}` endpoint. This page displays a success message and a button. Clicking on the button will redirect the browser to `${redirectUri}?updatedProperty=${updatedProperty}`.

The page hosted at `${redirectUri}` is under your control. This system allows PolicyGate to notify your application when the account management flow has finished, either with a successful or erroneous result.
