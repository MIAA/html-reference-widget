# PolicyGate HTML reference templates
## Table of Contents
1. [Introduction](#introduction)
2. [Terminology](#terminology)
2. [Technical info](#technical-info)
3. [Interaction flows](#interaction-flows)
3. [Account flows](#account-flows)
3. [Device code flow](#device-code-flow)
4. [Error handling](#error-handling)
___

## Introduction
This repository is the home of the reference templates to be used in combination with PolicyGate. For each of the flows PolicyGate offers, it renders one or a series of specific templates for each action in that flow. These templates are written in the [ejs](https://ejs.co/) syntax. We've listed the flows below along with the corresponding template and anatomy of the URL.
___

## Terminology
To avoid confusion, here are some of the terms employed by PolicyGate that are used throughout this document:

- Traditional account: an account that has a password set
- Social account: an account without a password that is linked to one or more social identity providers.
- Social provider: an identity provider, often a social network site such as, but not limited to: Google, Facebook, LinkedIn, Twitter, DocCheck, SwissRX, ...
- Social profile: the result of logging in with a social provider. This profile should contain at minimum a social provider ID (`$PID`) and the email of the user.
- interactionDetails: the information that PolicyGate provides to the template for rendering. This can include information about the current interaction like which screen the user can visit. It also includes screen-specific information and error details.
___

## Technical info
PolicyGate uses the [ejs](https://ejs.co/) rendering engine to transform dynamic "*.ejs" files to static HTML before serving it to the browser.
In this repository you will find a basic example for each of the templates that should help you get started on developing your own.

When PolicyGate is about to render a template it will include a set of relevant details to the `ejs` engine. You can use these `interactionDetails` to customize your templates or to pass on the information to javascript so you can use a framework of your choice to create your UI. For more information on which information is provided on each screen look at the relevant docs for the screen you're working on under [Interaction flows](docs/interaction-flows.md).

Most screens that have a user interaction will require you to configure a simple html form with a couple of mandatory fields. This form then needs to be submitted ("POST") to the same url on which it was served to trigger the next steps in the user flow. If something goes wrong when submitting the form then PolicyGate will render the original template again with included error details of what went wrong.

For any action directly related to the user profile, PolicyGate uses ProfileConnect as an intermediate interface between the information it receives and the user profile in the user store. For this reason the validation and mapping logic present in ProfileConnect needs to be taken into account when developing templates for PolicyGate. This is especially relevant for the "registration", "social registration" and "invitation" flows as there you can directly control how a new user profile is created.
___

## Interaction flows
This section talks about all of the screens and user journeys that are considered an *interaction*. An interaction is defined as any user flow that starts with the user being unauthenticated and possibly results in the user being authenticated and authorized to continue.
See [Interaction flows](docs/interaction-flows.md) for more info.
___

## Account flows
This section talks about user account specific actions such as changing your password. These actions require the user to be authenticated before starting the user flow.
See [Account flows](docs/account-flows.md) for more info.
___

## Device code flow
This section talks about the [device code grant](https://datatracker.ietf.org/doc/html/rfc8628#section-3.4) implementation for PolicyGate.
See [Device code flow](docs/device-code-flow.md) for more info.
___

## Error handling
Whenever PolicyGate encounters an unrecoverable error it renders the [error.ejs](./src/error.ejs) template. This allows you to provide a custom error screen to your end-users in cases where PolicyGate can not continue with the request.

Most of these types of errors can be caught and resolved during development.

For reference here are some examples of unrecoverable errors:

- Passing an invalid redirect URI to the `/auth` request.
- PolicyGate can't find the template that it wants to render.
- Navigating to a PolicyGate url of which the interaction has already expired.
- Navigating to a PolicyGate url which isn't recognized by PolicyGate.

You template can investigate the `error` variable, which will contain an
`error` and a `description` field.
